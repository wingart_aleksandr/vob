;(function($){

	"use strict";

	var Core = {

		DOMReady: function(){

			var self = this;

			self.menu.init();
			self.footer.init();
			self.firstScreen();
		},

		windowLoad: function(){

			var self = this;
			
			self.preloader();
			
		},

		/**
		**	FirstScreen
		**/

		firstScreen: function(){
			function screen(){
				if($('.first_screen').length){
					var headerH = $('header').outerHeight();

					$('.first_screen').css({
						'margin-top': -headerH
					});
				}
			}
			screen();

			$(window).on('resize',  function() {
				screen();
			});
		},

	   	/**
		**	Menu
		**/

		menu: {
			init: function(){
				var self = this;
				
				self.dropdown();
				self.event();
			},

			dropdown: function(){

				$('.menu_item').each(function(index, el){
					if($(el).find('.dropdown_box').length){
						$(el).addClass('has_dropdown');
						$(el).find('.menu_link').append('<i class="fa fa-angle-right"></i>');
					}
				});

			},

			event: function(){

				$('.js-btns-menu-mob').on('click', function(event) {
					$('body').toggleClass('showMenu scrollNo');
					$(this).toggleClass('active');
				});

				$('.menu_link').on('click', '.fa', function(event) {
					$(this).closest('.menu_link').toggleClass('active');
					$(this).closest('.menu_item').find('.dropdown_box').toggleClass('showDropdown').slideToggle(400);
				});

			}

		},

		/**
		**	footer
		**/

		footer: {
			init: function(){
				var self = this;
				
				self.dropdown();
				self.event();
			},

			dropdown: function(){

				$('.footer_box').each(function(index, el){
					if($(el).find('.footer_list').length){
						$(el).addClass('footer_has_dropdown');
						$(el).find('.footer_title').append('<i class="fa fa-angle-right"></i>');
					}
				});

			},

			event: function(){

				$('.footer_title').on('click', '.fa', function(event) {
					$(this).closest('.footer_title').toggleClass('active');
					$(this).closest('.footer_box').toggleClass('active').find('.footer_list').toggleClass('showDropdown').slideToggle(400);
				});

			}

		},

		/**
		**	Preloader
		**/

		preloader: function(){

			var self = this;

			self.preloader = $('#page-preloader');
	        self.spinner   = self.preloader.find('.preloader');

		    self.spinner.fadeOut();
		    self.preloader.delay(350).fadeOut('slow');
		},

	}


	$(document).ready(function(){

		Core.DOMReady();

	});

	$(window).load(function(){

		Core.windowLoad();

	});

})(jQuery);