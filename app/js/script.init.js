$(document).ready(function(){


	/* ------------------------------------------------
	Owl START
	------------------------------------------------ */

			if($('.js-logo-slider').length){
				$('.js-logo-slider').owlCarousel({
				    loop:false,
				    nav:true,
					navText: [
						'<span aria-label="' + 'Previous' + '"></span>',
						'<span aria-label="' + 'Next' + '"></span>'
					],
					responsive:{
				        0:{
				            items:1,
				            dots: true,
				        },
				        480:{
				            items:2,
				        },
				        576:{
				            items:3
				        },
				        768:{
				            items:3,
				            dots: false
				        },
				        992:{
				            items:5,
				            dots: false
				        },
				        1200:{
				            items:6,
				            dots: false
				        }
				    }
				});
			}

			if($('.js-gallery-slider').length){
				if($(window).width() <= 767){
					$('.js-gallery-slider').owlCarousel({
					    items:1,
					    loop:false,
					    nav:false,
					    dots: true,
						navText: [
							'<span aria-label="' + 'Previous' + '"></span>',
							'<span aria-label="' + 'Next' + '"></span>'
						]
					});
				}
			}

	/* ------------------------------------------------
	Owl END
	------------------------------------------------ */

	/* ------------------------------------------------
	PhotoAlbum START
	------------------------------------------------ */

			if($('.js-photo-album').length){
				lc_lightbox('.js-photo-album', {
					wrap_class: 'lcl_fade_oc',
					gallery : true,	
					thumb_attr: 'data-lcl-thumb', 
					thumbs_w    : 170,
					thumbs_h    : 114,
					max_width : '100%',
					show_title : false,
					show_descr : false,
					show_author : false,
					skin: 'minimal',
					radius: 0,
					padding	: 0,
					border_w: 0,
				});	
			}

	/* ------------------------------------------------
	PhotoAlbum END
	------------------------------------------------ */


});

$(window).load(function() {});